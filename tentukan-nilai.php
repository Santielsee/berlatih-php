<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
        function tentukan_nilai($number)
        {
            //  kode disini
            if($number < 60){
                $hasil = "Kurang";
            }else if($number >= 60 && $number <70){
                $hasil = "Cukup";
            }else if($number >= 70 && $number <85){
                $hasil =  "Baik";
            }else if($number >= 85 && $number <= 100){
                $hasil ="Sangat Baik";
            }
            return $hasil;
        }

//TEST CASES
?>
  <?php echo tentukan_nilai(98); ?>  
  <?php echo tentukan_nilai(76); ?>  
  <?php echo tentukan_nilai(67); ?>  
  <?php echo tentukan_nilai(43); ?>  
</body>
</html>

